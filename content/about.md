+++
title = "About"
date = "2005-05-20T15:34:07-04:00" 
menu = "main"
+++

<div class="aside">
<a href="/imgs/NissaEamesResume.pdf" target="_blank">
<img src="/imgs/resume.gif"><br>
Download my resume</a>
<hr>
<strong>Contact me</strong><br>
<a href="mailto:&#110;&#105;&#115;&#115;&#97;&#64;&#110;&#105;&#115;&#115;&#97;&#46;&#110;&#101;&#116;
">&#110;&#105;&#115;&#115;&#97;&#64;&#110;&#105;&#115;&#115;&#97;&#46;&#110;&#101;&#116;
</a><br><a href="tel:&#55;&#53;&#55;&#45;&#53;&#54;&#49;&#45;&#48;&#48;&#50;&#51;">&#55;&#53;&#55;&#45;&#53;&#54;&#49;&#45;&#48;&#48;&#50;&#51;</a>
</div>

# About me

## What I can do for you
I have a wide variety of skills in that I am both a designer and front-end developer. I am as familiar with Photoshop and Illustrator as I am with HTML, CSS, and Javascript, honing my knowledge over the last 16+ years. My current technology focus is on static site generators, and responsive design. I am versed in Drupal 7 and WordPress CMS theming and development, adding a general knowledge of PHP and MySQL to my skillset. I also have a deeply-ingrained work ethic and work well independently or with a team.

In addition, I am also experienced in graphic design, icon illustration, lettering &amp; calligraphy, and photography.  

## How I think

My goals for a website are always first-and-foremost ease-of-use and a high-quality aesthetic that promotes the brand of the product. The hierarchy of information is paramount. We need to answer important questions: what is our goal? What do our users need to know? How can we help them get what they want? A continual back-and-forth discussion with the client helps things to stay on the right track.

I love to learn and I am constantly researching new technologies and new uses for old ones. Watching design trends keeps me current and interested in new layouts and thought processes for creating website designs.

## What other people think

Over the years, I’ve received numerous awards for several websites I’ve worked on either as a team member or solo. The awards are listed in my portfolio and and on my resume.

## What I’m doing now

I currently work full-time for the Colonial Williamsburg Foundation as Senior Web Developer. In my life outside of work, taking care of my family and a life-long love of art and photography keep me busy.

## Things I like

I enjoy organizing and planning websites, apps, and utilites, creating beautiful, functional designs, and building the sites/apps to work quickly and seamlessly for the user.

Interests: mindfulness, psychology, natural horsemanship, healthy eating, gardening, home beautification and organization, calligraphy &amp; lettering, illustration, and photography


