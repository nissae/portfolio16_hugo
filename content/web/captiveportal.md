+++
draft = true

title = "Colonial Williamsburg's WiFi Access Portal"
date = "2015-02-20T15:34:07-04:00" 

projecttypes = ["web design", "front-end web development"]
roles = ["Art direction", "Design", "Structure coding (HTML, CSS)", "Scripting functionality (jQuery, ColdFusion, XML)"]
technologies = ["Node.js","HTML5", "CSS3", "jQuery",  "Responsive"]
projecturl = ""
awards = ""

heroimg = "/imgs/"

[[project_highlights]]
title = "Fully responsive"
text = "Naturally this had to cover mobile phones and laptops. Using the CSS viewport unit, I was able to create a functional display with very little mobile-specific styling."

[[project_highlights]]
title = "Friendly user interface using SVG"
text = "This was my first time creating a button that actually gives feedback when clicked. It morphs into a progress circle and then displays a green check or red X depending on the results. Oh yeah, fancy."

[[project_highlights]]
title = "Learning my way around Node.js"
text = "I was hoping to have some ultra-fancy SVG page transitions from one screen to another. After much trial and error it was not to be. I did learn a lot about Node on the way though."

+++

This is the portal that displays when visitors to Colonial Williamsburg's Historic Area Williamsburg try to access the free WiFi. Built in Node.js by IT's Database Development group, I was responsible for creating the front-facing display and functionality.

<a href="/imgs/histthreads-square.jpg" class="gallery-item" >Open image</a>

<h3>My history with history.org</h3>
[envira-gallery id="2386"]