+++
draft = false

title = "Mapping Colonial America"
subtitle = "Showcasing Colonial Williamsburg's map collection"
date = "2005-06-20T15:34:07-04:00" 

projecttypes = ["web design", "web development"]
roles = ["Art direction", "Design", "Structure coding (Flash)", "Scripting functionality (ActionScript2)"]
technologies = ["Flash"]
projecturl = "http://www.history.org/maps"
awards = "2006 Official Honoree of Annual Webby Awards, International Academy of Digital Arts & Sciences; 2006 Gold Award, W³ Awards; 2005 Outstanding Website WebAward, Web Marketing Association"
imgfolder = "mapping"

heroimg = "maps-square.jpg"

[[galleryimg]]
caption = "Introduction page"
src="maps1-firstmap.jpg"

[[galleryimg]]
caption = "Map zoom"
src="maps2-zoom.jpg"

[[galleryimg]]
caption = "Section introduction"
src="maps3-intro.jpg"

[[project_highlights]]
title = "Rich design"
text = "It may be a bit crowded compared to today's standards, but back then it was fairly standard. The deep red contributes to a sense of history while the pop-out map highlights the current feature."
src = "design.jpg"

[[project_highlights]]
title = "Integrated navigation and timeline"
text = "Using lots of fun, custom ActionScript, I created a drag-and-drop timeline. Once dragged-and-dropped, the historical text timeline and the active map animate to the same date, and vice versa. All of the animation in the exhibit was done using ActionScript."
src = "timeline.jpg"

[[project_highlights]]
title = "Preset zoom inset into text"
text = "Using Zoomify, I was able to provide inline links to view parts of the map referenced in the text."
src = "zoomlinks.jpg"

+++

This is an aged project, but I still really love it. It's an award-winning online exhibit, built in Flash, that provides a showcase for Colonial Williamsburg's collection of antique maps. An interactive timeline and zoom feature allow for in-depth learning. CDs were also produced for retail sale with an additional "Compare to Modern Map" feature.


