+++
draft = false

title = "Let Freedom Ring Challenge website"
subtitle = "Informational website with registration system"
date = "2015-06-20T15:34:07-04:00" 

projecttypes = ["web design", "front-end web development"]
roles = ["Design", "Structure coding (HTML, CSS)", "Scripting functionality (jQuery)"]
technologies = [".NET", "Knockout.js","HTML5", "CSS3", "jQuery", "Responsive"]
projecturl = "https://www.letfreedomringchallenge.org/"
awards = ""
imgfolder = "lfr"

heroimg = "home.jpg"

[[galleryimg]]
caption = "Home page"
src="home.jpg"

[[galleryimg]]
caption = "Date selection interface"
src="registration1_date.jpg"

[[galleryimg]]
caption = "Time availability interface"
src="registration2_time.jpg"

[[galleryimg]]
caption = "Information capture interface"
src="registration3_info.jpg"


[[project_highlights]]
title = "Reservation interface as work of interactive art"
text = "Ok, ok, it's not fine art, but the amount of effort I put in to make this the easiest, most-pleasant-to-use interface ever seen by man can not be understated. First, we choose the date. I chose a calendar format to make it straightforward and days that are not available are grayed out. "
src="registration1_date.jpg"

[[project_highlights]]
title = "Next we select a time"
text = "Oh the beauty. We show how many spots are available. Once you chose a spot a delightful check appears and it automatically asks for your name, to save a step in the next page."
src="registration2_time.jpg"

[[project_highlights]]
title = "And finally, we enter information"
text = "My goal here was to make it as easy as possible for the user to know if something was entered correctly. As the user types feedback is displayed below the field and once correctly entered yet another delightful checkbox makes us feel good about Doing Something Right For Once."
src="registration3_info.jpg"

+++

Colonial Williamsburg teamed up with First Baptist Church of Williamsburg to challenge the nation to ring their newly restored bell for the entire month of February 2016 (and beyond). The back-end of the site was built by IT's Database Development group and my job was to create the front-facing display and interaction. Bootstrap was used as the HTML/CSS framework (which is ok, but I actually prefer Foundation) and Knockout.js to display the data. 


