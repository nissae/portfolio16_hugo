+++
draft = false

title = "Colonial Williamsburg Education Resource Libary"
subtitle = "Drupal 7 site with content access"
date = "2016-06-20T15:34:07-04:00" 
employer = "Colonial Williamsburg" 

projecttypes = ["web design", "web development"]
roles = ["Design", "Structure coding (HTML, CSS, Drupal)", "Scripting functionality (jQuery, PHP)"]
technologies = ["Drupal 7","HTML5", "CSS3", "jQuery", "Responsive"]
projecturl = "http://resourcelibrary.history.org"
awards = ""
imgfolder = "resourcelibrary"

heroimg = "design.jpg"

[[galleryimg]]
caption = "Home page"
src="design.jpg"

[[galleryimg]]
caption = "Page showing categorized resources"
src="era_lg.jpg"

[[galleryimg]]
caption = "Video program page"
src="videoprogram.jpg"

[[galleryimg]]
caption = "Teacher Institute page"
src="documents.jpg"

[[project_highlights]]
title = "Pared down to the basics"
text = "With the generous amount of resources available, I chose to limit my design to the basics to reduce the user's mental load."
src="design.jpg"

[[project_highlights]]
title = "Custom module to display resources"
text = "I love the Views module, I do, but in this case it just couldn't handle three different content types, two different taxonomies, and grouping every which way from Sunday. Needless to say, I created my own module to display the three content types, nicely separated, and grouped by title, era, subject, or theme."
src="era_open.jpg"

[[project_highlights]]
title = "Provided access control"
text = "The Teacher Institute materials can only be accessed by Teacher Institute partcipants. To enable that I used the Content Access, Rules, and Registration Code modules to enable per-node access."
src="access_lg.jpg"

+++

Created as way for teachers to access Colonial Williamsburg's vast library of educational media. 

