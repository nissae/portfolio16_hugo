+++
draft = false

title = 'Coins & Currency Online Exhbit'
subtitle = "Showcasing Colonial Williamsburg's coin collection"
date = "2007-06-20T15:34:07-04:00" 

projecttypes = ["web design", "web development"]
roles = ["Art direction", "Design", "Structure coding (Flash)", "Scripting functionality (ActionScript2, XML)"]
technologies = ["Flash", "XML/XSLT"]
projecturl = "http://www.history.org/history/museums/coinExhibit/"
awards = "2007 Interactive Media Awards: Outstanding Achievement; 2007 Silver Award, W³ Awards; 2007 Outstanding Website WebAward, Web Marketing Association"
imgfolder = "coins"

heroimg = "coins_square.jpg"

[[galleryimg]]
caption = "Introduction page"
src="coins1_intro.jpg"

[[galleryimg]]
caption = "Section introduction screen"
src="coins2_sectionintro.jpg"

[[galleryimg]]
caption = "Section introduction screen"
src="coins3_mainpage.jpg"

[[galleryimg]]
caption = "Section introduction screen"
src="coins4_moreinfo.jpg"

[[galleryimg]]
caption = "Section introduction screen"
src="coins5_timeline.jpg"

[[galleryimg]]
caption = "Section introduction screen"
src="coins6_zoom1.jpg"

[[galleryimg]]
caption = "Section introduction screen"
src="coins7_zoom.jpg"

[[galleryimg]]
caption = "Section introduction screen"
src="coins8_search.jpg"

[[galleryimg]]
caption = "Section introduction screen"
src="coins9_printable.jpg"

[[project_highlights]]
title = 'Easy to update using XML'
text = 'Once again I turned to my trusted back-end format of XML to enable text and image changes without having to re-export the whole exhibit from Flash when content changed.'
src="xml.jpg"

[[project_highlights]]
title = 'Super-fancy dynamically created timeline'
text = 'This was a hard one! Pulled information from the XML file and then set it up to dynamically arrange on the timeline.'
src="coins5_timeline.jpg"

[[project_highlights]]
title = 'Lots of added features'
text = 'To make the exhibit as useful as possible, I included a glossary, search, zoom functionality, informational popups, relative size, and relative value displays.'
src="coins6_zoom1.jpg"

+++
An online exhibit built to showcase rare coins from the Colonial Williamsburg collection.

This is an aging project of mine. I still enjoy all the little details that went in to making it a valuable educational tool. About the time I finished, Javascript had reached a point where the whole thing could have been created using it. Ah well. 
