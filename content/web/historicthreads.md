+++
draft = false

title = "Historic Threads Online Exhibit"
subtitle = "XML-based responsive online exhibit"
date = "2011-06-20T15:34:07-04:00" 

projecttypes = ["web design", "web development"]
roles = ["Art direction", "Design", "Structure coding (HTML, CSS)", "Scripting functionality (jQuery, ColdFusion, XML)"]
technologies = ["ColdFusion","HTML5", "CSS3", "jQuery", "XML/XSLT", "Responsive"]
projecturl = "http://www.history.org/history/museums/clothingexhibit/index.cfm"
awards = "Communicator Awards: Interactive Excellence"
imgfolder = "histthreads"

heroimg = "histthreads.jpg"

[[galleryimg]]
caption = "Introduction page"
src="histthreads1-intro.jpg"

[[galleryimg]]
caption = "Explore the artifacts page"
src="histthreads2-explore.jpg"

[[galleryimg]]
caption = "Filter interface"
src="histthreads3-filter.jpg"

[[galleryimg]]
caption = "Learning section"
src="histthreads4-learn.jpg"


[[project_highlights]]
title = 'Built to be easy to update'
text = 'Using XML with a combination of XSLT and ColdFusion, I was able to create an easy-to-alter back-end for future additions or changes.'
src="xml_lg.jpg"

[[project_highlights]]
title = 'Responsive design'
text = 'This may have been my first responsive project. I used the excellent framework known as Foundation.'
src="responsive_lg.jpg"

[[project_highlights]]
title = 'Filter and search'
text = 'Once again using XML, XSLT, and ColdFusion, I was able to create both a filtering system and a search feature to allow researchers to narrow down the display of artifacts.'
src="filter_lg.jpg"


+++

An online exhibit of antique garments; features responsive design, zoom, filtering, and search capabilities.
