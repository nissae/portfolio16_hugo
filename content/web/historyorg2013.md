+++
draft = false

title = "History.org Redesign 2013"
subtitle = "ColdFusion-based, popular colonial history site"
date = "2013-06-20T15:34:07-04:00" 

projecttypes = ["web design", "web development"]
roles = ["Art direction", "Collaborative design", "Structure coding (HTML, CSS)", "Scripting functionality (jQuery, ColdFusion, XML)"]
technologies = ["ColdFusion","HTML5", "CSS3", "jQuery",  "XML/XSLT", "Responsive"]
projecturl = "http://www.history.org"
awards = "2013, 2012, 2011, 2010, 2009, 2008, 2007, 2006 and 2005 Outstanding Website WebAward, Web Marketing Association; 2011, 2010, 2009, 2008, 2006 Gold Award, W³ Awards"
imgfolder = "historyorg13"

heroimg = "2013historyorg1a-home.jpg"

[[galleryimg]]
caption = "Home page"
src="2013historyorg1a-home.jpg"

[[galleryimg]]
caption = "History section landing page"
src="2013historyorg3-historylanding.jpg"

[[galleryimg]]
caption = "Inside page of History section"
src="2013historyorg4-historyinside.jpg"

[[galleryimg]]
caption = "Museum section landing page"
src="2013historyorg5-museumslanding.jpg"

[[galleryimg]]
caption = "Inside Museum page"
src="2013historyorg6-museumsublanding.jpg"

[[galleryimg]]
caption = "Inside Museum page"
src="2013historyorg7-museumsinside.jpg"

[[galleryimg]]
caption = "Development section landing page"
src="2013historyorg8-devellanding.jpg"

[[galleryimg]]
caption = "Inside Development page"
src="2013historyorg9-develinside.jpg"

[[project_highlights]]
title = "Updated design"
text = "Streamlined the home page and landing pages to enable a more direct path to the user's main area of interest. Created templates for a wide variety of layouts including landing and sublanding pages, multimedia and video, and topic filter pages. "
src="2013historyorg1a-home.jpg"

[[project_highlights]]
title = "Created CMS-style features"
text = "Before the option of using a CMS was available, I created various systems to enable similar features on the site such as dynamic navigation and related links. Using XML, XSLT, and ColdFusion, I created a dynamic, easy-to-update navigation system to deal with the unwieldy number of sections and links of the site. Related links content in the sidebar is populated by numerous XML files using a set of common tags."
src="2013historyorg4-historyinside.jpg"

[[project_highlights]]
title = "Created modular CSS components"
text = "To enable consistent styling and easier page creation, I created reusable CSS components to be used in the site. A styleguide showed all of the options available, and included lessons and exercises on XML and XSLT."
src="2013historyorg7-museumsinside.jpg"

[[project_highlights]]
title = 'Mobile template'
text = 'Created a unique homepage for mobile visitors, and a mobile template for the inside pages of the site. '
src="mobile_lg.jpg"

+++

History.org is a very popular (over 4 million unique views a year) education-focused website with ten major sections and a vast body of high-quality content. The site's audiences include the general public, teachers and students, Historic Area visitors, donors, and history lovers.

<!--h3>My history with history.org</h3-->