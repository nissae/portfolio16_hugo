+++
draft = false

title = "CW Today brochure mockup design"
date = "2016-06-20T15:34:07-04:00" 
employer = "Colonial Williamsburg" 

projecttypes = ["Graphic design", "Print", "Brochure"]
imgfolder = "cwtoday"

heroimg = "cwtoday_square.jpg"

[[galleryimg]]
src = "cwtoday1.jpg"
caption= "Outside"

[[galleryimg]]
src = "cwtoday2.jpg"
caption= "Inside"


+++

Mockup brochure created for daily Colonial Williamsburg handout to visitors. 
