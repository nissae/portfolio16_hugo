+++
draft = false

title = "Icons for Diversions website"
date = "2016-06-20T15:34:07-04:00" 
employer = "Savannah Mornings News" 

projecttypes = ["Graphic design", "Print", "Icons"]
imgfolder = "divicons"

heroimg = "divicons-square.jpg"

[[galleryimg]]
src = "divicons1.gif"
caption= ""



+++

A group of icons created for a local entertainment website.
