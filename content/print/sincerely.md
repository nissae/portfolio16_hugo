+++
draft = false

title = "Sincerely CD design"
date = "2016-06-20T15:34:07-04:00" 
employer = "Harry O'Donoghue" 

projecttypes = ["Graphic design", "Print", "CD"]
imgfolder = "sincerely"

heroimg = "sincerely-square.jpg"

[[galleryimg]]
src = "sincerely1.jpg"
caption= "Front, back and disc"

[[galleryimg]]
src = "sincerely2.jpg"
caption= "Inside panels"

[[galleryimg]]
src = "odonoghue_shirt2.jpg"
caption= "A shirt made for the project"

+++

Created for Irish folksinger Harry O'Donoghue.
