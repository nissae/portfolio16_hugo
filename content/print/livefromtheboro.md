+++
draft = false

title = "Live from the 'Boro CD design"
date = "2016-06-20T15:34:07-04:00" 
employer = "Harry O'Donoghue" 

projecttypes = ["Graphic design", "Print", "CD"]
imgfolder = "livefromtheboro"

heroimg = "livefromtheboro1_square.jpg"

[[galleryimg]]
src = "livefromtheboro1_square.jpg"
caption= "Front"

[[galleryimg]]
src = "livefromtheboro2-back.jpg"
caption= "Back"

[[galleryimg]]
src = "livefromtheboro3-disc.jpg"
caption= "Disc"

+++

Created for Irish folksinger Harry O'Donoghue.
