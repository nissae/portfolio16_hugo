+++
draft = false

title = "Logo design"
date = "2016-06-20T15:34:07-04:00" 
employer = "Various" 

projecttypes = ["Graphic design", "Print", "Logo"]
imgfolder = "logos"

heroimg = "suncatcherlogo.gif"

[[galleryimg]]
src = "suncatcherlogo.gif"
caption= "Suncatchers, logo for solar house architecture design company"

[[galleryimg]]
src = "memoryboxlogo2_square.jpg"
caption= "Memory Box Photograohy, logo for personal lifestyle photo company"

[[galleryimg]]
src = "mangocatlogo-square.jpg"
caption= "Mangocat Designs, logo for imaginary design company"



+++

A group of logos for a solar design company, portrait company, and design firm.
