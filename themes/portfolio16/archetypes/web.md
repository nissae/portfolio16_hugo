+++
draft = false

title = ""
date = "2016-06-20T15:34:07-04:00" 
employer = "Colonial Williamsburg" 

projecttypes = ["web design", "web development"]
roles = ["design", "Structure coding (HTML, CSS, Drupal)", "Scripting functionality (jQuery, PHP)"]
technologies = ["HTML5", "CSS3", "jQuery", "Drupal 7","responsive"]
projecturl = "http://resourcelibrary.history.org"
awards = ""
imgfolder = ""

heroimg = ""

[[galleryimg]]
url = ""
caption= ""

[[project_highlights]]
title = ""
text = ""
src = ""

[[project_highlights]]
title = ""
text = ""
src = ""

[[project_highlights]]
title = ""
text = ""
src = ""

+++

History.org is a popular education-focused website with ten major sections and a vast body of high-quality content. The site's audiences include the general public, teachers and students, Historic Area visitors, donors, and history lovers.
