+++
draft = false

title = ""
date = "2016-06-20T15:34:07-04:00" 
employer = "" 

projecttypes = ["graphic design", "print"]
imgfolder = ""

heroimg = ""

[[galleryimg]]
url = ""
caption= ""

+++

History.org is a popular education-focused website with ten major sections and a vast body of high-quality content. The site's audiences include the general public, teachers and students, Historic Area visitors, donors, and history lovers.
